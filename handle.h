/* $Id$ $URL$ */
#ifndef HANDLE_DOT_AITCH
#define HANDLE_DOT_AITCH
/**\file handle.h
 * \ingroup oohandle
 * Casses to implement a reference counting OO object handle
 * \author Trent Apted <tapted@it.usyd.edu.au>
 */

#ifdef _MSC_VER
#ifdef HANDLE_EXPORTING
#define HANDLE_API __declspec(dllexport)
#else
#define HANDLE_API __declspec(dllimport)
#endif
#else
#define HANDLE_API //nothing
#endif

#include <exception>
#include <string>
#include <cstddef>
#include <ostream>
#include <cstdio>

#include <typeinfo>

/**\def NDEMANGLE
 * If defined, do not attempt to demangle type names
 */

#ifndef __GCC_ABI_VERSION
#define NDEMANGLE
#endif

#ifndef NDEMANGLE
#include <cxxabi.h>
#endif

#define BOOST_NO_CONFIG
#define BOOST_HAS_THREADS
#include <boost/detail/atomic_count.hpp>

using std::size_t; //everyone uses it..

/**
 * This is thrown if someone uses the template CC or AO
 * and the dynamic_cast fails. We inherit from bad_cast,
 * which is only thrown when a dynamic_cast is used to
 * initialise a reference -- whereas we throw one whenever a
 * dynamic cast fails.
 */
#pragma warning(suppress: 4275) // MSVC: non dll-interface class 'std::bad_cast' used as base for dll-interface class 'BadCast'
class HANDLE_API BadCast : public std::bad_cast {
protected:
    enum {
        MAX_INFO_LEN = 256 ///< Maximum description length / buffer size
    };
    /** Buffer for the description string */
    char info[MAX_INFO_LEN+1]; //we keep this here in case we get sliced
    /** Create a BadCast */
    BadCast() throw() {info[0] = info[MAX_INFO_LEN] = '\0';}
public:
    /** Return a nice description of what caused this exception to be thrown */
    virtual const char *what() const throw() {return info;}
};

/**
 * Imlementation of BadCast using templates to determine
 * type information for exception::what() -- you shouldn't
 * catch this -- catch BadCast.
 */
template <class LHS, class RHS>
class HANDLE_API BadCastImpl : public BadCast {
public:
    /** Generate a BadCast for particular types */
    BadCastImpl(const RHS &rhs, const char* extra = "") throw() {
#ifdef NDEMANGLE
        /* note we use the TYPE LHS (not the object lhs) */
        (void)snprintf(info, MAX_INFO_LEN,
                       "%s (Cannot assign an object of run-time type %s to lvalue of type %s%s)",
                       std::bad_cast::what(), typeid(rhs).name(), typeid(LHS).name(), extra);
#else
        int status;
        char *real_except = abi::__cxa_demangle(std::bad_cast::what(), 0, 0, &status);
        char *real_rhs = abi::__cxa_demangle(typeid(rhs).name(), 0, 0, &status);
        char *real_LHS = abi::__cxa_demangle(typeid(LHS).name(), 0, 0, &status);
        (void)snprintf(info, MAX_INFO_LEN,
                       "%s (%s): Cannot assign an object of run-time type %s (%s) to lvalue of type %s (%s)%s",
                       real_except, std::bad_cast::what(), real_rhs, typeid(rhs).name(), real_LHS, typeid(LHS).name(), extra);
        free(real_except);
        free(real_rhs);
        free(real_LHS);
#endif

    }
};

using boost::detail::atomic_count;

/**
 * Your standard reference counting handle with some
 * extra tiddly bits. Makes polymorphism and heap-
 * constructed objects easy to manage in C++.
 */
template <class T>
class Handle {
protected:
    template <class C> friend class Handle;
    T* p;         ///< The pointer we are counting
    atomic_count* refs; ///< Pointer to the number of references to \a p

    /**
     * "Safe" assignment, that increments first so that
     * Handles can be used in tree structures. Also means we don't
     * have to check for self-assignment.
     */
    void safeAssign(atomic_count* newRefs) throw() {
        atomic_count* oldRefs(refs);
        ++*newRefs;
        refs = newRefs;
        if (!--*oldRefs) {
            delp();
            delete oldRefs;
        }
    }

    /** Delete the pointer */
    virtual void delp() throw() {delete p;}
public:
    /**
     * Construct a <tt>new T()</tt> with \a T's default constructor.
     */
    Handle () throw(std::bad_alloc) : p(new T()) , refs(new atomic_count(1)) {}

    /**
     * Attach \a pp to a new Handle. \a pp is assumed to have no aliases.
     * The best way to use this constructor is with
     * \code
     * Handle<T> hand(new T(...));
     * \endcode
     * This is explicit because a pointer passed to a function accepting
     * handles would cause the pointer to be deleted on return. A good
     * strategy is to have functions accept pointers as arguments, but
     * return Handles.
     * \param pp The pointer to attach
     */
    explicit Handle (T* pp) throw(std::bad_alloc) : p(pp), refs(new atomic_count(1)) {}

    /** Return the number of references to the pointer */
    size_t numRefs() {return *refs;}

    /**
     * Copy constructor (increments the reference count).
     * \note C++ will make it's own CC rather than use the template
     * \param h the Handle to copy
     */
    Handle (const Handle<T>& h) throw() : p(h.p), refs(h.refs) {
        ++*refs;
    }

    /**
     * Templatized copy constructor for polymorphic behaviour.
     * \param h the Handle to copy
     * \throws BadCast if the run-time type of h is not a decendant of T
     */
    template <class C>
        Handle (const Handle<C>& h) throw(BadCast)
        :
    p(dynamic_cast<T*>(h.p)), refs(&++*h.refs) {
        if (!p) throw BadCastImpl<T, C>(*h.p, " in Handle copy constructor");
    }

    /**
     * Assignment operator -- safely deletes/assigns to an existing handle.
     * \param h the Handle to assign.
     * \return *this
     */
    Handle& operator= (const Handle<T>& h) throw() {
        safeAssign(h.refs);
        p = h.p;
        return *this;
    }

    /**
     * Assignment operator for an un-Handle<>d object. For this case,
     * we assume that we are going similar to:
     *
     * \code
     * Handle<T> x;
     * x = new T();
     * \endcode
     *
     * \param np the unhandled pointer to start handling
     * \return *this
     */
    Handle& operator= (T* np) throw(std::bad_alloc) {
        if (p == np) return *this;
        return *this = Handle<T>(np);
    }

    /**
     * Templatized assignment operator for polymorphic behaviour.
     * \note likewise the Assignment operator; this is a bit less efficient.
     * \param h the Handle to copy
     * \throws BadCast if the run-time type of h is not a decendant of T
     */
    template <class C>
    Handle& operator= (const Handle<C>& h) throw(BadCast) {
        safeAssign(h.refs);
        p = dynamic_cast<T*>(h.p);
        if (!p) throw BadCastImpl<T, C>(*h.p, " in Handle assignment operator");
        return *this;
    }

    /**
     * Reference counting destructor.
     */
    virtual ~Handle() throw() {
        if (!--*refs) {
            delp();
            delete(refs);
        }
    }

    /**
     * This just returns the underlying pointer.
     * \note const semantics are based on the Handle: not the underlying
     * pointer. For const semantics on the pointer itself, use a
     * Handle<const T>.
     */
    T* operator-> () const throw() {
        return p;
    }

    /**
     * Dereferencing operator returns a reference to the underlying object.
     */
    T& operator* () const throw() {
        return *p;
    }

    /**
     * Return the underlying pointer.
     */
    T* ref() const throw() {
        return p;
    }

    /**
     * Allow automatic conversions to the pointer. This seems to behave OK.
     */
    operator T*() const throw() {
        return p;
    }

    /** Equality based on pointer comparison -- do we reference the same object */
    template <class C>
    bool operator==(const Handle<C> &rhs) const throw() {
        return p == rhs.ref();
    }
};

/**
 * A Handle for the array versions of new/delete. Note the way this uses
 * inheritance, so is actually fully compatible with the non-array versions.
 * Except, client code for the non-array versions probably won't know that
 * it's an array, so would probably always use only the first element. Note
 * that there can still be problems with deletion here if slicing occurs AND
 * the last Handle to be deleted was sliced to the non-array version. Beware.
 */
template <class T>
class HANDLE_API AHandle : public Handle<T> {
protected:
    /** For AHandle, delp calls <tt>delete[]</tt> */
    virtual void delp() {delete[] Handle<T>::p;}
public:
    /**
     * Creates an array of size \a sz and attaches it to the handle.
     * \param sz The size of the array to create.
     */
    explicit AHandle (size_t sz = 1) throw (std::bad_alloc) : Handle<T>(new T[sz]) {}
    /**
     * Attaches a handle to an already constructed array -- p must be
     * allocated using new[] otherwise there will be problems.
     */
    AHandle (T* p) throw(std::bad_alloc) : Handle<T>(p) {}
    /**
     * Handy indexation operator.
     * \return <tt>p[idx]</tt>
     */
    T& operator[](size_t idx) const throw() {return Handle<T>::p[idx];}
};

/**\example handletester.cc*/

/**
 * This is the easy way to be explicit about converting a T* to a Handle<T>
 */
template <class T>
Handle<T> make (T* p) throw(std::bad_alloc) {
    return Handle<T>(p);
}

/**
 * For Handle output, we automatically defer to an output operator for the target type
 * (assuming one exists).
 */
template <class T>
std::ostream& operator<<(std::ostream &o, const Handle<T> &h) {
    return o << *h;
}

#endif
