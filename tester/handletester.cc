#include "handle.h"

#include <iostream>

class A {public: virtual ~A() {}};
class B : public A {};
class C : public B {};


int main() {
    Handle<A> a;
    Handle<B> b;
    Handle<C> c;

    try {

        a = b; //upcast
        b = a; //downcast, OK
        c = a; //downcast, BAD
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    try  {
        Handle<B> bb;
        Handle<A> aa(bb); //upcast
        Handle<B> bbb(aa); //downcast, OK
        Handle<C> cc(aa); //downcast, BAD
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    try {
        Handle <A> aa;
        Handle <const A> ac;
        ac = aa;
        //aa = ac; compile error -- can't cast away constness
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
